import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Test2 {

    /*
    o Скрипт Б. Проверка работоспособности главного меню Админ панели
    1. Войти в Админ панель (по аналогии с предыдущим скриптом)
    2. Кликнуть на каждом видимом пункте главного меню (Dashboard, Заказы, Каталог, Клиенты...) для открытия соответствующего раздела и выполнить следующее:
    a. Вывести в консоль заголовок открытого раздела.
    b. Выполнить обновление (рефреш) страницы и проверить, что пользователь остается в том же разделе после перезагрузки страницы.
    */

    private static WebDriver driver = Utils.getInitFirefoxDriver();

    public static void main(String[] args) {

        driver.manage().window().maximize();

        LoginPage loginPage = new LoginPage(driver);

        loginPage.openPage();

        loginPage.doLogin();

        WebElement ulElement = driver.findElement(By.className("menu"));
        List<WebElement> liElements = ulElement.findElements(By.tagName("li"));
        liElements = liElements.stream()
                .filter(webElement -> webElement.getAttribute("class").contains("maintab"))
                .collect(Collectors.toList());
        List<String> ids = new ArrayList<>();

        for (WebElement element:
             liElements) {
            String id = element.getAttribute("id");
            if(id!=null){
                ids.add(id);
            }
        }

        ids.forEach(Test2::click);

        Utils.wait(2);

        driver.quit();
    }

    private static void click(String id){
        WebElement element = driver.findElement(By.id(id));
        element.click();
        Utils.wait(1);
        String currentPage = driver.getTitle();
        System.out.println("-------------------------------------");
        System.out.println("Current page:["+currentPage+"]");
        driver.navigate().refresh();
        String newPage = driver.getTitle();
        if(!newPage.equals(currentPage)){
            System.out.println("Page was changed after refresh!");
            System.out.println("Before page: ["+currentPage+"], new page["+newPage+"]");
        }
        /*
        вынужденная мера. Поскольку при переключении на разные пункты меню
        происходит смена классов и id пунктов меню.
        */
        if(!id.equals("tab-AdminDashboard")){
            driver.navigate().back();
        }
        Utils.wait(1);
    }
}
