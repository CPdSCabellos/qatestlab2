import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Test1 {

    /*
    o Скрипт А. Логин в Админ панель
    1. Открыть страницу Админ панели
    2. Ввести логин, пароль и нажать кнопку Логин.
    3. После входа в систему нажать на пиктограмме пользователя в верхнем правом углу и выбрать опцию «Выход.»
    */

    public static void main(String[] args) {
        WebDriver driver = Utils.getInitFirefoxDriver();
        driver.manage().window().maximize();

        LoginPage loginPage = new LoginPage(driver);

        loginPage.openPage();

        loginPage.doLogin();

        WebElement infoBtn = driver.findElement(By.className("employee_avatar_small"));
        infoBtn.click();

        Utils.wait(2);
        WebElement logoutBtn = driver.findElement(By.id("header_logout"));
        logoutBtn.click();

        Utils.wait(1);

        driver.quit();
    }
}
