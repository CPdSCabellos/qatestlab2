import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage {

    private final WebDriver driver;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }

    /**
     * Открыть страницу Админ панели
     */
    public void openPage(){
        driver.navigate().to(Utils.getProperty("mainURL"));

        Utils.wait(3);
    }


    /**
     * Ввести логин, пароль и нажать кнопку Логин.
     */
    public void doLogin(){
        WebElement loginField = driver.findElement(By.id("email"));
        loginField.sendKeys(Utils.getProperty("login"));

        WebElement passwordField = driver.findElement(By.id("passwd"));
        passwordField.sendKeys(Utils.getProperty("password"));

        WebElement loginBtn = driver.findElement(By.name("submitLogin"));
        loginBtn.submit();

        Utils.wait(3);
    }
}
