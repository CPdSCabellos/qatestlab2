import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.ResourceBundle;

public class Utils {

    private static ResourceBundle resourceBundle = ResourceBundle.getBundle("DefaultProperties");

    public static String getProperty(String propertyName){
        return resourceBundle.getString(propertyName);
    }

    public static  WebDriver getInitFirefoxDriver(){
        System.setProperty("webdriver.gecko.driver", Utils.class.getResource("geckodriver.exe").getPath());
        return new FirefoxDriver();
    }

    public static void wait(int seconds){
        try {
            Thread.sleep(seconds*1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
